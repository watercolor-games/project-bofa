namespace Bofa
{
    /// <summary>
    /// The entity layers provided by BofaComponent.
    /// </summary>
    public enum Layers
    {
        /// <summary>
        /// The level backdrop.
        /// </summary>
        BackgroundLayer,

        /// <summary>
        /// Players, characters, collision data.
        /// </summary>
        GameplayLayer,

        /// <summary>
        /// World elements that should be in front of the action.
        /// </summary>
        ForegroundLayer,

        /// <summary>
        /// The HUD and menus.
        /// </summary>
        UILayer,

        /// <summary>
        /// Not a layer - gives the amount of layers.
        /// </summary>
        Count
    }
}

