using Plex.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Input.InputListeners;
using Plex.Engine.GraphicsSubsystem;
using Plex.Engine;
using Microsoft.Xna.Framework.Graphics;
using Plex.Engine.Themes;
using Plex.Engine.GUI;
using Microsoft.Xna.Framework.Audio;
using Plex.Engine.Cutscene;
using Plex.Engine.Saves;
using Plex.Engine.Server;
using Microsoft.Xna.Framework.Content;

namespace Bofa
{
    /// <summary>
    /// Controls the game layers.
    /// </summary>
    public class BofaComponent : IEngineComponent
    {
        [Dependency]
        private Plexgate plexgate = null;
        
        private Layer[] layers = null;

        /// <inheritdoc/>
        public void Initiate()
        {
            layers = new Layer[(int)Layers.Count];
            for (int i = 0; i < layers.Length; i++)
                plexgate.AddLayer(layers[i] = new Layer());
            setupMenu();
        }

        /// <summary>
        /// Gets the Layer instance for a Layers enum entry.
        /// </summary>
        public Layer GetLayer(Layers layer)
        {
            return layers[(int)layer];
        }

        private void setupMenu()
        {
            // ok, so not actually a MENU yet
            GetLayer(Layers.GameplayLayer).AddEntity(plexgate.New<Deezus>());
        }

        public void Reset()
        {
            for (int i = 0; i < layers.Length; i++)
                layers[i].ClearEntities();
            setupMenu();
        }
    }
}
