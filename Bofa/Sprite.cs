﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Input.InputListeners;
using Plex.Engine.GraphicsSubsystem;
using Plex.Engine.Interfaces;
using Microsoft.Xna.Framework.Graphics;

namespace Bofa
{
    public abstract class Sprite : IEntity, IBox
    {
        Texture2D imgf;
        bool init = false;
        protected Texture2D img
        {
            get
            {
                return imgf;
            }
            set
            {
                imgf = value;
                if (!init && value != null)
                {
                    rect = new System.Drawing.Rectangle(0, 0, value.Width, value.Height);
                    init = true;
                }
            }
        }
        System.Drawing.Rectangle rect;
        public System.Drawing.Rectangle Rect
        {
            get
            {
                return rect;
            }
            set
            {
                rect = value;
                init = true;
            }
        }

        public void Draw(GameTime time, GraphicsContext gfx)
        {
            gfx.DrawRectangle(Rect.X, Rect.Y, Rect.Width, Rect.Height, img);
        }

        public abstract void OnKeyEvent(KeyboardEventArgs e);
        public abstract void OnMouseUpdate(MouseState mouse);
        public abstract void Update(GameTime time);
    }
}
