﻿using System;
using Microsoft.Xna.Framework;
namespace Bofa
{
    /// <summary>
    /// Objects implementing this have a position and size on a plane.
    /// </summary>
    public interface IBox
    {
        System.Drawing.Rectangle Rect { get; set; }
    }
}
