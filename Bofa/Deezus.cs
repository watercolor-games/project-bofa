﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Input.InputListeners;
using Plex.Engine.GraphicsSubsystem;
using Plex.Engine.Interfaces;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Drawing;

namespace Bofa
{
    public sealed class Deezus : Sprite, ILoadable, IDisposable
    {
        public void Load(ContentManager content)
        {
            img = content.Load<Texture2D>("Entities/Deezus");
        }

        public void Dispose()
        {
            img = null;
        }

        public override void OnKeyEvent(KeyboardEventArgs e)
        {
        }

        public override void OnMouseUpdate(MouseState mouse)
        {
        }

        public override void Update(GameTime time)
        {
        }
    }
}
